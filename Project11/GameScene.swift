//
//  GameScene.swift
//  Project11
//
//  Created by JONATHAN HOLLAND on 8/2/19.
//  Copyright © 2019 Jonny. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene, SKPhysicsContactDelegate {
    
    //MARK: Properties
    var playerBalls = 5 {
        didSet {
            playerBallsLabel.text = "Balls: \(playerBalls)"
        }
    }
    var playerBallsLabel: SKLabelNode!
    var onscreenBoxes = 0
    var onscreenBoxNode = [SKNode]()
    var scoreLabel: SKLabelNode!
    var score = 0 {
        didSet {
            scoreLabel.text = "Score: \(score)"
        }
    }//end of score property
    var editLabel: SKLabelNode!
    var editingMode: Bool = false {
        didSet {
            if editingMode {
                editLabel.text = "Done"
            } else {
                editLabel.text = "Edit"
            }
        }
    }//end of editingMode property
    // warningLabel alerts the user to tapping too low and a tip at the beginning of the game.
    var warningLabel: SKLabelNode!
    var warningMode: Bool = false {
        didSet {
            if warningMode {
                warningLabel.text = "Game Over"
            } else {
                warningLabel.text = "Tap to play"
            }
        }
    }//end of warningMode
    let availableBalls = ["ballBlue", "ballCyan", "ballGreen", "ballGrey", "ballPurple", "ballRed", "ballYellow"]
    
    override func didMove(to view: SKView) {
        let background = SKSpriteNode(imageNamed: "background.jpg")
        background.position = CGPoint(x: 512, y: 384)
        background.blendMode = .replace
        background.zPosition = -1
        addChild(background)
        physicsBody = SKPhysicsBody(edgeLoopFrom: frame)
        physicsWorld.contactDelegate = self
        makeSlot(at: CGPoint(x: 128, y: 0), isGood: true)
        makeSlot(at: CGPoint(x: 384, y: 0), isGood: false)
        makeSlot(at: CGPoint(x: 640, y: 0), isGood: true)
        makeSlot(at: CGPoint(x: 896, y: 0), isGood: false)
        makeBouncer(at: CGPoint(x: 0, y: 0))
        makeBouncer(at: CGPoint(x: 256, y: 0))
        makeBouncer(at: CGPoint(x: 512, y: 0))
        makeBouncer(at: CGPoint(x: 768, y: 0))
        makeBouncer(at: CGPoint(x: 1024, y: 0))
        beginningGameBoxes()
        scoreLabel = SKLabelNode(fontNamed: "Chalkduster")
        scoreLabel.text = "Score: 0"
        scoreLabel.horizontalAlignmentMode = .right
        scoreLabel.position = CGPoint(x: 980, y: 700)
        addChild(scoreLabel)
        playerBallsLabel = SKLabelNode(fontNamed: "Chalkduster")
        playerBallsLabel.text = "Balls: 5"
        playerBallsLabel.horizontalAlignmentMode = .right
        playerBallsLabel.position = CGPoint(x: 980, y: 650)
        addChild(playerBallsLabel)
        editLabel = SKLabelNode(fontNamed: "Chalkduster")
        editLabel.text = "Edit"
        editLabel.position = CGPoint(x: 80, y: 700)
        addChild(editLabel)
        warningLabel = SKLabelNode(fontNamed: "Chalkduster")
        warningLabel.text = "Add some blocks"
        warningLabel.horizontalAlignmentMode = .center
        warningLabel.position = CGPoint(x: 500, y: 700)
        addChild(warningLabel)
    }//end of didMove()
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let location = touch.location(in: self)
            let objects = nodes(at: location)
            if objects.contains(warningLabel) {
                beginningGameBoxes()
                playerBalls = 5
                score = 0
            } else if objects.contains(editLabel) {
                editingMode.toggle()
            } else if editingMode {
                // creat a box
                makeABox(position: randomPoints())
            } else if playerBalls > 0 {
                    if location.y > 600 {
                        // creat a ball
                        let ball = SKSpriteNode(imageNamed: availableBalls.randomElement() ?? "ballRed")
                        ball.physicsBody = SKPhysicsBody(circleOfRadius: ball.size.width / 2.0)
                        ball.physicsBody!.contactTestBitMask = ball.physicsBody!.collisionBitMask
                        ball.physicsBody?.restitution = 0.4
                        ball.position = location
                        ball.name = "ball"
                        warningLabel.text = "Tap to play"
                        playerBalls -= 1
                        addChild(ball)
                    } else {
                        warningLabel.text = "Must tap higher"
                    }
            } else if onscreenBoxes < 1 || playerBalls < 1 {
                warningMode.toggle()
            }//end of if objects.contains() & else statements
        }//end of if let touch {}
    }//end of touchesBegan()
    
    //MARK: funcs
    func beginningGameBoxes() {
        for _ in 0...20 {
            makeABox(position: randomPoints())
            onscreenBoxes += 1
        }
    }
    func makeABox(position: CGPoint) {
        let size = CGSize(width: Int.random(in: 16...128), height: 16)
        let box = SKSpriteNode(color: UIColor(red: CGFloat.random(in: 0...1), green: CGFloat.random(in: 0...1), blue: CGFloat.random(in: 0...1), alpha: 1), size: size)
        box.zRotation = CGFloat.random(in: 0...3)
        box.position = CGPoint(x: position.x, y: position.y)
        box.physicsBody = SKPhysicsBody(rectangleOf: box.size)
        box.physicsBody?.isDynamic = false
        box.name = "box"
        addChild(box)
    }
    func randomPoints() -> CGPoint {
        let randomXLocation = Int.random(in: 0...1024)
        let randomYLocation = Int.random(in: 100...550)
        return CGPoint(x: randomXLocation, y: randomYLocation)
    }
    func makeBouncer(at position: CGPoint) {
        let bouncer = SKSpriteNode(imageNamed: "bouncer")
        bouncer.position = position
        bouncer.physicsBody = SKPhysicsBody(circleOfRadius: bouncer.size.width / 2.0)
        bouncer.physicsBody?.isDynamic = false
        addChild(bouncer)
    }//end of makeBouncer()
    
    func makeSlot(at position: CGPoint, isGood: Bool) {
        var slotBase: SKSpriteNode
        var slotGlow: SKSpriteNode
        
        if isGood {
            slotBase = SKSpriteNode(imageNamed: "slotBaseGood")
            slotGlow = SKSpriteNode(imageNamed: "slotGlowGood")
            slotBase.name = "good"
        } else {
            slotBase = SKSpriteNode(imageNamed: "slotBaseBad")
            slotGlow = SKSpriteNode(imageNamed: "slotGlowBad")
            slotBase.name = "bad"
        }
        slotBase.position = position
        slotGlow.position = position
        slotBase.physicsBody = SKPhysicsBody(rectangleOf: slotBase.size)
        slotBase.physicsBody?.isDynamic = false
        addChild(slotBase)
        addChild(slotGlow)
        let spin = SKAction.rotate(byAngle: .pi, duration: 10)
        let spinForever = SKAction.repeatForever(spin)
        slotGlow.run(spinForever)
    }//end of makeSlot()
    
    func collisionBetween(ball: SKNode, object: SKNode) {
        if object.name == "good" {
            destroy(object: ball)
            score += 1
            playerBalls += 1
        } else if object.name == "bad" {
            destroy(object: ball)
            score -= 1
        } else if object.name == "box" {
            destroy(object: object)
            onscreenBoxes -= 1
        }
    }//end of collisionBetween()
    
    func destroy(object: SKNode) {
        if let fireParticles = SKEmitterNode(fileNamed: "FireParticles") {
            fireParticles.position = object.position
            addChild(fireParticles)
        }
        object.removeFromParent()
    }//end of destroy()
    
    func didBegin(_ contact: SKPhysicsContact) {
        guard let nodeA = contact.bodyA.node else { return }
        guard let nodeB = contact.bodyB.node else { return }
        
        if nodeA.name == "ball" {
            collisionBetween(ball: nodeA, object: nodeB)
        } else if nodeB.name == "ball" {
            collisionBetween(ball: nodeB, object: nodeA)
        } else if nodeA.name == "box" {
            collisionBetween(ball: nodeA, object: nodeB)
        } else if nodeB.name == "box" {
            collisionBetween(ball: nodeB, object: nodeA)
        }
    }//end of didBegin()
}//end of GameScene{}
